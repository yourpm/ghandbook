﻿using Microsoft.EntityFrameworkCore;
using RMVC_GHandbook.Models;

namespace RMVC_GHandbook.Data
{
    public class ApplicationDBContext:DbContext
    {
        // создаем контекст обращения к базам данных
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options):base(options)
        {

        }
        // добавляем нашу модель к базам данных с возможностями получения и записи
        public DbSet<GameMasterClass> Games { get; set; }
    }
}
