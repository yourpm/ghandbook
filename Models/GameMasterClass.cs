﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RMVC_GHandbook.Models
{
    // создадим модель данных, котора представляет собой объектно ориентированное представление для реляционной подели
    // укажем необходимые поля объекта (таблицы), их тип и другие параментры методами ComponentModel
    public class GameMasterClass
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string? Title { get; set; }
		[DisplayName("Description")]
		public string? TiDescription { get; set; }
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        public string? Genre { get; set; }
		[DisplayName("Price $")]
		[Range(0, 999999)] 
        public decimal Price { get; set; }
        
        public DateTime? AddDate { get; set; } = DateTime.Now;
    }
}
