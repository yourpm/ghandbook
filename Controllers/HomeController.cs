﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RMVC_GHandbook.Data;
using RMVC_GHandbook.Models;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace RMVC_GHandbook.Controllers
{
    // объявляем контроллер который будет обрабатывать запросы и возвращать представления 
    public class HomeController : Controller
    {
        // создаем средство ведения журнала для HomeController
        private readonly ILogger<HomeController> _logger;
        // создаем средство обращения к базе данных
        private readonly ApplicationDBContext _db;
        // вызываем созданные средства
        public HomeController(ILogger<HomeController> logger, ApplicationDBContext db)
        {
            _logger = logger;
            _db = db;
        }
        // генератор стартовой страницы index которая возвращает список видеоигр и их параметры
        public IActionResult Index(string? search)
        {
            // создаем список char которые фильтруются из запросов
			var dd = new char[] {'/','"','\'' };
            // получаем данные из базы данных как нумерованный массив
            IEnumerable<GameMasterClass> ObjGamesList = _db.Games.ToList();
            // если запрос не сожержит фильтров (пустой) возвращаем все данные
            if (search == null)
			{
                return View(ObjGamesList);
            }
            // убираем лишние символы из запроса
            search = search.Trim(dd);
            // создаем новый список данных
			List<GameMasterClass> resultr = new List<GameMasterClass>();
            // добавляем возможность административного вызова
            if (search.Contains("***"))
            {
                // убираем символы административного вызова
                search = search.Trim('*');
                // переводим пользователя на страницу для админов с переданным значением фильтров
                return RedirectToAction("Index", "Home4admin", new { search = search });
            }
            // для каждого элемента из базы данных проверяем на совпадения с фильтрами
            foreach (var item in ObjGamesList)
			{
                // если итем соответствует фильтру, сохраняем в новый массив
				if (item.Title.ToLower().Contains(search.ToLower()) || item.Genre.ToLower().Contains(search.ToLower()))
				{
                    resultr.Add(item);
                }	
			}
            // возвращаем новый массив данных
            return View(resultr);
        }
        // генератор страницы возвращающей все данные для конкретного итема
        public async Task<IActionResult> Details(int? id)
		{
            // если итема не существует возвращаем "не найдено"
            if (id == null || _db.Games == null)
            {
                return NotFound();
			}
            // находим первый или дефолтный результат из базы данных соответствующий id
            var gameMasterClass = await _db.Games
                .FirstOrDefaultAsync(m => m.Id == id);
            // если итема нет в базе данных возвращаем "не найдено"
            if (gameMasterClass == null)
            {
                return NotFound();
            }
            // в остальных случаях возвращаем итем
            return View(gameMasterClass);
        }
        // генератор страницы privacy
        public IActionResult Privacy()
        {
            return View();
        }
        // генератор страницы ошибок
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}