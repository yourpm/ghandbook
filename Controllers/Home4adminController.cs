﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RMVC_GHandbook.Data;
using RMVC_GHandbook.Models;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace RMVC_GHandbook.Controllers
{
    // объявляем контроллер который будет обрабатывать запросы и возвращать представления 
    public class Home4adminController : Controller
    {
        // создаем средство ведения журнала для Home4adminController
        private readonly ILogger<HomeController> _logger;
        // создаем средство обращения к базе данных
        private readonly ApplicationDBContext _db;
        // вызываем созданные средства
        public Home4adminController(ILogger<HomeController> logger, ApplicationDBContext db)
        {
            _logger = logger;
            _db = db;
        }
        // генератор стартовой страницы index которая возвращает список видеоигр и их параметры
        public IActionResult Index(string? search)
        {
            // создаем список char которые фильтруются из запросов
            var dd = new char[] {'/','"','\'' };
            // получаем данные из базы данных как нумерованный массив
            IEnumerable<GameMasterClass> ObjGamesList = _db.Games.ToList();
            // если запрос не сожержит фильтров (пустой) возвращаем все данные
            if (search == null)
			{
                return View(ObjGamesList);
            }
            // убираем лишние символы из запроса
            search = search.Trim(dd);
            // создаем новый список данных
            List<GameMasterClass> resultr = new List<GameMasterClass>();
            // для каждого элемента из базы данных проверяем на совпадения с фильтрами
            foreach (var item in ObjGamesList)
			{
                // если итем соответствует фильтру, сохраняем в новый массив
                if (item.Title.ToLower().Contains(search.ToLower()) || item.Genre.ToLower().Contains(search.ToLower()))
				{
                    resultr.Add(item);
                }	
			}
            // возвращаем новый массив данных
            return View(resultr);
        }
		// генератор страницы create реализующий форму сбора данных
		public IActionResult Create()
		{
			return View();
		}
		// обрабатываем данные из формы create и ссохраняем из в базу данных
		//POST
		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Create(GameMasterClass obj)
		{
            // получаем данные из базы данных как нумерованный массив
            IEnumerable<GameMasterClass> ObjGamesList = _db.Games.ToList();
			// для каждогоэлемента массива проверим существование элементов
            foreach (var item in ObjGamesList)
            {
				if (obj.Title.ToLower() == item.Title.ToLower()){ ModelState.AddModelError("Title","Already exists");}
			}
			// проверим является ли модель правильной и достаточной
            if (ModelState.IsValid)
			{
				// если указан жанр преобразуем символы в нижний регистр
                if (obj.Genre != null)
                {
                    obj.Genre = obj.Genre.ToLower();
                }
				// добавим данные в нумерованный массив
				_db.Games.Add(obj);
				// сохраним изменения в базе данных
				_db.SaveChanges();
				// добавим кастомный вывод сообщающий об успехе
				TempData["success"] = "Game created successfully";
				// перенаправим пользователя на стартовую страницу 
				return RedirectToAction("Index");
			}
			// в остальных случаях вернем модель (набор ошибок валидации)
			return View(obj);
		}
        // генератор страницы edit реализующий форму сбора данных для уже существующего элемента базы данных
        //GET
        public IActionResult Edit(int? id)
		{
			// если элемент не задан возвращаем "не найдено"
			if (id == null || id == 0)
			{
				return NotFound();
			}
			// получаем первый или дефолтный элемент из базы данных соответствующий id
			var gameFromDb = _db.Games.FirstOrDefault(u=>u.Id==id);
			// если элемент не найден в базе данных возвращаем "не найдено"
			if (gameFromDb == null)
			{
				return NotFound();
			}
			// возвращаем форму для текущего элемента
			return View(gameFromDb);
		}
        // обрабатываем данные из формы edit и сохраняем их в базу данных
        //POST
        [HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Edit(GameMasterClass obj)
		{
            // проверим является ли модель правильной и достаточной
            if (ModelState.IsValid)
			{
                // если указан жанр преобразуем символы в нижний регистр
                if (obj.Genre != null)
				{
					obj.Genre = obj.Genre.ToLower();
				}
                // изменяем данные в нумерованном массиве
                _db.Games.Update(obj);
                // сохраним изменения в базе данных
                _db.SaveChanges();
                // добавим кастомный вывод сообщающий об успехе
                TempData["success"] = "Game updated successfully";
                // перенаправим пользователя на стартовую страницу 
                return RedirectToAction("Index");
			}
            // в остальных случаях вернем модель (набор ошибок валидации)
            return View(obj);
		}
        // генератор страницы edit реализующий форму сбора данных для уже существующего элемента базы данных
        public IActionResult Delete(int? id)
		{
            // если элемент не задан возвращаем "не найдено"
            if (id == null || id == 0)
			{
				return NotFound();
			}
            // получаем первый или дефолтный элемент из базы данных соответствующий id
            var gameFromDb = _db.Games.FirstOrDefault(u=>u.Id==id);
            // если элемент не найден в базе данных возвращаем "не найдено"
            if (gameFromDb == null)
			{
				return NotFound();
			}
            // возвращаем форму для текущего элемента
            return View(gameFromDb);
		}
        // обрабатываем данные из формы delete и сохраняем их в базу данных
        //POST
        [HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public IActionResult DeletePOST(int? id)
		{
            // получаем первый или дефолтный элемент из базы данных соответствующий id
            var obj = _db.Games.FirstOrDefault(u=>u.Id==id);
            // если элемент не найден в базе данных возвращаем "не найдено"
            if (obj == null)
			{
				return NotFound();
			}
            // удаляем данные в нумерованном массиве
            _db.Games.Remove(obj);
            // сохраним изменения в базе данных
            _db.SaveChanges();
            // добавим кастомный вывод сообщающий об успехе
            TempData["success"] = "Game deleted successfully";
            // перенаправим пользователя на стартовую страницу 
            return RedirectToAction("Index");
		}
        // генератор страницы возвращающей все данные для конкретного итема
        public async Task<IActionResult> Details(int? id)
		{
            // если итема не существует возвращаем "не найдено"
            if (id == null || _db.Games == null)
            {
                return NotFound();
			}
            // находим первый или дефолтный результат из базы данных соответствующий id
            var gameMasterClass = await _db.Games
                .FirstOrDefaultAsync(m => m.Id == id);
            // если итема нет в базе данных возвращаем "не найдено"
            if (gameMasterClass == null)
            {
                return NotFound();
            }
            // в остальных случаях возвращаем итем
            return View(gameMasterClass);
        }
        // генератор страницы ошибок
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}