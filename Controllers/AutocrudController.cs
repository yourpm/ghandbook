﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RMVC_GHandbook.Data;
using RMVC_GHandbook.Models;

namespace RMVC_GHandbook.Controllers
{
    // пример автоматически сгенерированного кода контроллера для модели  GameMasterclass
    public class AutocrudController : Controller
    {
        // средство обращения к базе данных
        private readonly ApplicationDBContext _context;
        // вызываем созданные средства
        public AutocrudController(ApplicationDBContext context)
        {
            _context = context;
        }
        // аналог Home4adminController Index без функций поиска (фильтров)
        // GET: Autocrud
        public async Task<IActionResult> Index()
        {
              return View(await _context.Games.ToListAsync());
        }
        // аналог Home4adminController Details
        // GET: Autocrud/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Games == null)
            {
                return NotFound();
            }

            var gameMasterClass = await _context.Games
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gameMasterClass == null)
            {
                return NotFound();
            }

            return View(gameMasterClass);
        }
        // аналог Home4adminController Create
        // GET: Autocrud/Create
        public IActionResult Create()
        {
            return View();
        }
        // аналог Home4adminController Create POST
        // POST: Autocrud/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,TiDescription,ReleaseDate,Genre,Price,AddDate")] GameMasterClass gameMasterClass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(gameMasterClass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(gameMasterClass);
        }
        // аналог Home4adminController Edit
        // GET: Autocrud/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Games == null)
            {
                return NotFound();
            }

            var gameMasterClass = await _context.Games.FindAsync(id);
            if (gameMasterClass == null)
            {
                return NotFound();
            }
            return View(gameMasterClass);
        }
        // аналог Home4adminController Edit POST
        // POST: Autocrud/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,TiDescription,ReleaseDate,Genre,Price,AddDate")] GameMasterClass gameMasterClass)
        {
            if (id != gameMasterClass.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gameMasterClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameMasterClassExists(gameMasterClass.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(gameMasterClass);
        }
        // аналог Home4adminController Delete
        // GET: Autocrud/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Games == null)
            {
                return NotFound();
            }

            var gameMasterClass = await _context.Games
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gameMasterClass == null)
            {
                return NotFound();
            }

            return View(gameMasterClass);
        }
        // аналог Home4adminController Delete POST
        // POST: Autocrud/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Games == null)
            {
                return Problem("Entity set 'ApplicationDBContext.Games'  is null.");
            }
            var gameMasterClass = await _context.Games.FindAsync(id);
            if (gameMasterClass != null)
            {
                _context.Games.Remove(gameMasterClass);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        // метод проверки существования значения в базе данных поиск по id
        private bool GameMasterClassExists(int id)
        {
          return _context.Games.Any(e => e.Id == id);
        }
    }
}
