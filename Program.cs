﻿using RMVC_GHandbook.Data;
using Microsoft.EntityFrameworkCore;
//вызов метода CreateBuilder который возвращает экземпляр WebApplicationBuilder содержащий свойства сборки проекта
//настройки журнала для WebApplication хранятся в файле appsettings.json
var builder = WebApplication.CreateBuilder(args);
//добавляем контейнеры сервисов к проекту 
//AddControllersWithViews сервис обработки представлений контроллерами
builder.Services.AddControllersWithViews();
//AddDbContext сервис подключения к базе данных
//настройки строки подключения к базе данных хранятся в файле appsettings.json
builder.Services.AddDbContext<ApplicationDBContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
//сборка проекта
var app = builder.Build();
//проверка переменной среды отвечающей за статус приложения
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
//добавим возможность работы со скриптами и другими файлами из статических файлов (по умолчанию wwwroot)
app.UseStaticFiles();
//включение маршрутизации запросов настроим в MapControllerRoute
app.UseRouting();
//добавим поддержку аунтификации с помощью cookie
app.UseAuthorization();
//настроим маршруты запросов по умолчанию
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
//запустим настроенное приложение
app.Run();